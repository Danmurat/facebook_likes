# README #

This is a small python script which uses the Facebook Graphs Api to retrieve and display the total likes you have received on everything you have posted to Facebook.
It also displays a league table of your top "Likers".

### How do I get set up? ###

* Not essential but PyCharm is a great editor for Python... And its free - https://www.jetbrains.com/pycharm/
* First you need Python 2.7 installed on your computer
* You will also need to install Conda - https://conda.io/docs/user-guide/install/index.html#regular-installation
* Once Conda is installed you will need to create your environment using the fb_environment.yml file included in this package (env name facebook_likes) - https://conda.io/docs/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file
* Now activate your environment https://conda.io/docs/user-guide/tasks/manage-environments.html#activating-an-environment
* You will need to open the main.py file and change graph = facebook.GraphAPI(access_token="ENTER FACEBOOK ACCESS TOKEN HERE") by adding your access token which can be obtained here: https://developers.facebook.com/tools/explorer/
* After updating the main.py file you are ready to go... just run "python main.py" from a terminal window.

### IF YOU HAVE ISSUES WITH CONDA ###
* The only dependancy is facebook sdk. So all you need to do is install Python 2.7 and then PIP install facebook and you're ready to go.

### Who do I talk to? ###

* dmurat.com