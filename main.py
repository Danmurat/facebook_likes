import facebook
import time
import operator
import webbrowser
import os

likes = 0  # How many times they likes on your posts


# Generate html content and save as likes.html
def update_html(total_likes, sorted_likes):
    code_str = str(total_likes)
    digits = list(code_str)

    add_zeros = 8 - len(digits)

    if add_zeros > 0:
        for x in range(0, add_zeros):
            digits.insert(0, '0')

    html_file = open('html/likes.html', 'w')

    most_likes_table = ""

    for order in sorted_likes:
        most_likes_table += "<tr><td class='base position'>%s</td><td class='base name'>%s</td><td class='base val'>%s</td></tr>" % (sorted_likes.index(order) + 1, order[0].encode('utf-8').strip(), str(order[1]))

    likes_content = """<html>
    <head>
        <link href="styles.css" type="text/css" rel="stylesheet">
    </head>
    <body>
    <div class="container"> 
    <img class="thumbsUp" src="like_img.jpg" />
    <div class="allNumbers">
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
        <div class="aNumber">%s</div>
    </div>
    </div>
    <br clear="both" />
    <table class="theTable">
    %s
    </table>
    </body>
    </html>""" % (digits[-8], digits[-7], digits[-6], digits[-5], digits[-4], digits[-3], digits[-2], digits[-1], most_likes_table)

    html_file.write(likes_content)
    html_file.close()


# Get post data and extract likes information
def check_for_likes():
    global likes
    # Generate access Token - https://developers.facebook.com/tools/explorer
    graph = facebook.GraphAPI(access_token="ENTER FACEBOOK ACCESS TOKEN HERE")
    posts = graph.get_connections("me", "posts?fields=likes.limit(300)&limit=1000")

    total_likes = 0

    # Fetch data for posts that have been made by the user. Push the name of each person that has 'liked' to all_names
    all_data = posts['data']

    all_names = []
    for one_post in all_data:
        try:
            total_likes += len(one_post['likes']['data'])
            for x in range(0, len(one_post['likes']['data'])):
                all_names.append(one_post['likes']['data'][x]['name'])
        except:
            print('No likes on this post ' + one_post['id'])

    # Take list of every name that has liked a post and create a new list which only contains each name once.
    tmp_names = []
    for name in all_names:
        if name not in tmp_names:
            tmp_names.append(name)

    # Create a dictionary linking each name with the total number of likes they have made.
    most_likes = {}
    for name_count in tmp_names:
        # print(name_count)
        most_likes.update({name_count: all_names.count(name_count)})

    # print(most_likes)
    # Sort the list by highest number of likes ready to be passed to the update_html() function
    sorted_high = sorted(most_likes.items(), key=operator.itemgetter(1), reverse=True)

    if total_likes > likes:
        likes = total_likes
        update_html(total_likes, sorted_high)
        webbrowser.get().open_new('file://' + os.path.realpath('html/likes.html'))
        # print(total_likes)
    else:
        print('No New Likes')

    time.sleep(60)
    check_for_likes()


check_for_likes()
